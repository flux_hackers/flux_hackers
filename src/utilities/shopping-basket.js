const basketKey = 'shopping-basket';

const createEmptyBasket = (name) => {
    setItems(name, []);
};

const setItems = (name, items) => {
    window.sessionStorage.setItem(basketKey, JSON.stringify({ [name]: { items }}));
};

const getItems = (name) => {
    if (!window.sessionStorage.getItem(basketKey) || !JSON.parse(window.sessionStorage.getItem(basketKey))[name]) {
        createEmptyBasket(name);
    }
    return JSON.parse(window.sessionStorage.getItem(basketKey))[name].items;
};

const getScore = (name) => {
    const basketItems = getItems(name);
    let totalScore = 0;
    basketItems.map(item => {
        let score = parseInt(item.points, 10);
        totalScore += score;
        return item;
    });

    return totalScore;
};

const getTotal = (name) => {
    const basketItems = getItems(name);
    let totalPrice = 0;
    basketItems.map(item => {
        let price = parseFloat(item.price, 10);
        totalPrice += price;
        return item;
    });

    return totalPrice.toFixed(2);
};

const addItem = (name, item) => {
    let items = getItems(name);
    item.id = items.length + 1;
    items.push(item);
    setItems(name, items);
};

const removeItem = (name, item) => {
    let items = getItems(name);
    setItems(name, items.filter(anItem => anItem.id !== item.id));
};

const ShoppingBasket = {
    addItem,
    getItems,
    getScore,
    getTotal,
    removeItem,
    emptyBasket: createEmptyBasket
};

export { ShoppingBasket };

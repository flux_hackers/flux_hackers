import React from 'react';
import { Route } from 'react-router-dom';
import { Intro } from '../../routes/intro/intro';
import { LoggedIn } from '../../routes/logged-in/logged-in';

const Template = () => (
    <React.Fragment>
        <Route exact path="/" component={Intro}/>
        <Route path="/:name" component={LoggedIn} />
    </React.Fragment>
);

export { Template }

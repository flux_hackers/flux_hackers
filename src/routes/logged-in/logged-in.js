import React from 'react';
import { Route } from 'react-router-dom';
import { Header } from '../../components/header/header';
import { Basket } from '../basket/basket';
import { Grocery } from '../grocery/grocery';
import { Offers } from '../offers/offers';
import { Summary } from '../summary/summary';

const LoggedIn = ({ match }) => (
    <React.Fragment>
        <Header name={match.params.name} />
        <Route path="/:name/grocery" component={Grocery} />
        <Route path="/:name/offers" component={Offers} />
        <Route path="/:name/summary" component={Summary} />
        <Route path="/:name/basket" component={Basket} />
    </React.Fragment>
);

export { LoggedIn }
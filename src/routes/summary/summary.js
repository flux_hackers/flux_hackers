import React from 'react';
import './summary.css';
import DataStore from '../../utilities/data-store'
import { ShoppingBasket } from '../../utilities/shopping-basket';
import cross from '../../images/cross.gif';
import tick from '../../images/tick.png';
import logo from '../../images/cheese-grater.png';
import candle from '../../images/candle.jpg';

const getName = (name) => name.charAt(0).toUpperCase() + name.slice(1);
const getScore = (name) => ShoppingBasket.getScore(name);
const getBasketTotal = (name) => ShoppingBasket.getTotal(name);
const getBasketSavings = (name) => DataStore.results[name].savings;
const getGoodStuff = (name) => DataStore.results[name].positives;
const getBadStuff = (name) => DataStore.results[name].negatives;

const Summary = (props) => (
    <div>
        <div className="headerContainer">
            <h1 className="Summary-title">Summary</h1>
            <img className="logo" src={logo}/>
        </div>
        <div className="mainSiteContainer">
            <div className="Summary-score">
                <p>Hi {getName(props.match.params.name)} your score is... {getScore(props.match.params.name)}</p>
                <p>Your basket total was £{getBasketTotal(props.match.params.name)}</p>
                <div>
                    <p>You saved £{getBasketSavings(props.match.params.name)}, with this you could buy...</p>
                    <p>Aromatic "Exotic Mango" Room Scenter, Paraffin Wax, Orange Candle</p>
                    <img className="Summary-canBuyImage"  src={candle} />
                </div>
            </div>
            <div className="Summary-doneWell">
                <h2>You did these things well</h2>
                {getGoodStuff(props.match.params.name).map((goodThing, i) => (
                    <div className="Summary-info" key={i}>
                        <p className="Summary-infoTitle">
                            <img className="Summary-icon" src={tick}/>
                            {goodThing.title}
                        </p>
                        <p>{goodThing.description}</p>
                    </div>
                ))}
            </div>
            <div className="Summary-doBetter">
                <h2>You could have picked better value here</h2>
                {getBadStuff(props.match.params.name).map((badThing, i) => (
                    <div className="Summary-info" key={i}>
                        <p className="Summary-infoTitle">
                            <img className="Summary-icon" src={cross}/>
                            {badThing.title}
                        </p>
                        <p>{badThing.description}</p>
                    </div>
                ))}
            </div>
        </div>
    </div>
);

export { Summary };

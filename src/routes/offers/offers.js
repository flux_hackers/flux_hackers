import React from 'react';
import logo from '../../images/cheese-grater.png';

const Offers = () => (
    <React.Fragment>
        <div className="headerContainer">
            <h1>Offers</h1>
            <img className="logo" src={logo}/>
        </div>
        <div className="mainSiteContainer">
            <p>offers placeholder</p>
        </div>
    </React.Fragment>
);

export { Offers }
import PropTypes from 'prop-types';
import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import dataStore from '../../../utilities/data-store';
import { ShoppingList } from '../shopping-list/shopping-list';
import arrowRight from '../../../images/arrowRight.png';
import './groceryInfo.css';

class GroceryInfo extends React.Component {
    render() {
        const match = this.props.match;

        return (
            <React.Fragment>
                <p className="groceryInfo-budget">Your budget is: £6.50</p>
                <div className="groceryShoppingListContainer">
                    <p className="text">This is a list of items that you brie-d to buy.</p>
                    <img className="arrowRight" src={arrowRight}/>
                    <ShoppingList onListChange={() => this.forceUpdate()}/>
                </div>
                {GroceryInfo.renderFinishedLink(match)}
                <p>You will not have enough money to buy the brie-mium versions of all products.</p>
                <p>If you stil(ton) can't afford the products you want then extra money is available to you, but you
                    feta pay it back! You'd b-Emmental not to!</p>
                <p>The better the value, the better score you will get. Cheese.</p>
            </React.Fragment>
        );
    }
}

GroceryInfo.renderFinishedLink = (match) => {
    if (!dataStore.areAllTheGroceryItemsMarkedOff()) {
        return null;
    }
    return (<Link to={`/${match.params.name}/summary`} className="groceryInfo-finishedLink">Finished</Link>);
};

GroceryInfo.propTypes = {
    match: PropTypes.object
};

const GroceryInfoWithRouter = withRouter(GroceryInfo);

export default GroceryInfoWithRouter;

import React from 'react';
import './search.css';

class Search extends React.Component {
    constructor(props) {
        super(props);
        this.state = {value: ''};
        this.items = props.items;
        this.onFilterChange = props.onFilterChange;

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value});

        this.onFilterChange(this.items.filter(item => {
            const myRegex = new RegExp(event.target.value, 'ig');
            return myRegex.test(item.name);
        }));
    }

    render() {
        return (
            <div>
                <form>
                    <label>Search Items</label>
                    <input id="search" type="search" value={this.state.value} onChange={this.handleChange} />
                </form>
            </div>
        );
    }
}

export { Search }

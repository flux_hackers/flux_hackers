import PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom';
import './header.css';

const Header = ({ name }) => (
    <ul className="Header-list">
        <li className="Header-listItem">
            <Link className="Header-link" to={`/${name}/grocery`}>Grocery</Link>
        </li>
        <li className="Header-listItem">
            <Link className="Header-link" to={`/${name}/basket`}>View Basket</Link>
        </li>
        <li className="Header-listItem">
            <Link className="Header-link" to="/">Logout</Link>
        </li>
    </ul>
);

Header.propTypes = {
    name: PropTypes.string.isRequired
};

export { Header }

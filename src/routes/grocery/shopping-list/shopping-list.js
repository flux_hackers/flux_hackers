import PropTypes from 'prop-types';
import React from 'react';
import { ShoppingListItem } from './shopping-list-item';
import './shopping-list.css';
import dataStore from '../../../utilities/data-store';

const notifyIfListComplete = (onListChange) => {
    if (onListChange) {
        onListChange();
    }
};

const ShoppingList = ({ onListChange }) => {
    return (
        <div className="ShoppingList-Container">
            <h2>Shopping List</h2>
            {
                dataStore.groceryItems.map(item => item.isOnList && (
                    <ShoppingListItem key={item.id} item={item} isComplete={() => notifyIfListComplete(onListChange)}/>
                ))
            }
        </div>
    );
};

ShoppingList.propTypes = {
    onListChange: PropTypes.func
};

export { ShoppingList }

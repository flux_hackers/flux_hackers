import groceryItems from '../data/groceryItemCollection.json';
import results from '../data/results.json'

class DataStore {
    constructor() {
        this.groceryItems = groceryItems;
        this.results = results;
    }

    areAllTheGroceryItemsMarkedOff() {
        return this.groceryItems.filter(anItem => anItem.isOnList === true).every((item) => item.checked);
    }
}

const dataStore = new DataStore();

export default dataStore;

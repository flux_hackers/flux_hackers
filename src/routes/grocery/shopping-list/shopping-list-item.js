import PropTypes from 'prop-types';
import React from 'react';

const toggleCheckbox = (cb, isComplete) => {
    cb.checked = !cb.checked;

    const labelElement = document.querySelector(`label.${cb.id}`);

    if (cb.checked) {
        labelElement.classList.add('cross-off');
    } else {
        labelElement.classList.remove('cross-off');
    }

    if (isComplete) {
        isComplete(cb.checked);
    }
};

const ShoppingListItem = ({ item, isComplete }) => {
    item.checked = item.checked || false;

    return (
        <div>
            <input type="checkbox" defaultChecked={item.checked} id={item.id} onChange={() => {
                toggleCheckbox(item, isComplete);
            }} />
            <label htmlFor={item.id} className={`${item.id}${item.checked ? ' cross-off' : ''}`}>{item.displayName}</label>
        </div>
    );
};

ShoppingListItem.propTypes = {
    item: PropTypes.shape({
        id: PropTypes.string,
        displayName: PropTypes.string
    }),
    isComplete: PropTypes.func
};

export { ShoppingListItem };

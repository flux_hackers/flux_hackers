1. Login as Sarah
2. Click on groceries
3. Search for Cheddar
    3.1	Add normal cheddar to basket
    3.2 Check it off the list
4. Search for bananas
    4.2 Add bagged bananas to basket
    4.3 Check it off the list
5. Search for eggs
    5.1 Add 6 Free range eggs to basket
    5.2 Spot the 12 Free range eggs
    5.3 Add them too
    5.4 Remove 6 free range eggs
    5.5 Check it off the list
6. Search for milk
    6.1 Add 4 pints of milk
    6.2 Check it off the list
7. View basket to check total
8. Go bak to shopping list
9. Click finished button
    9.1 Talk through summary

import PropTypes from 'prop-types';
import React from 'react';
import './displayItems.css';
import { withRouter } from 'react-router-dom';
import DataStore from '../../utilities/data-store';
import { ShoppingBasket } from '../../utilities/shopping-basket';
import { Search } from "./search/search";

class DisplayItems extends React.Component {
    constructor(props) {
        super(props);

        const filteredItems = [].concat.apply([], DataStore.groceryItems.map((item) => item.items));

        this.state = {
            items: props.items || filteredItems,
            filteredItems: props.items || filteredItems
        };
        this.isBasketPage = props.isBasketPage;
        this.match = props.match;
        this.onFilterChange = this.onFilterChange.bind(this);
    }

    componentDidUpdate(prevProps) {
        if (this.props.items !== prevProps.items) {
            this.setState({
                filteredItems: this.props.items
            })
        }
    }

    onFilterChange(items) {
        this.setState({
            filteredItems: items
        });
    }

    render() {
        return (
            <div className="DisplayItems-Container">
                <h2>Items</h2>
                <Search items={this.state.items} onFilterChange={this.onFilterChange} />
                <div className="displayItemContainer">
                    {
                        this.state.filteredItems.map((item, i) => {

                            let img;

                            if (item.image) {
                                img = require(`../../images/${item.image}`);
                            }

                            return (
                                <div className="displayItemItem" key={i}>
                                    <h3 tabIndex="-1">{item.name}</h3>
                                    <p>£{item.price}</p>
                                    <div className="displayItemsImgContainer">
                                        {img && <img className="displayItems-img" src={img}/>}
                                    </div>
                                    {
                                        this.isBasketPage
                                        ? <button onClick={() => {
                                            ShoppingBasket.removeItem(this.match.params.name, item);
                                            this.onFilterChange(ShoppingBasket.getItems(this.match.params.name));
                                            document.querySelector('#search').focus();
                                        }}>Remove Item</button>
                                        : <button onClick={() => ShoppingBasket.addItem(this.match.params.name, item)}>Add Item</button>
                                    }
                                </div>
                            )
                        })
                    }
                </div>
            </div>
            
        )
    }
}

export { DisplayItems }


DisplayItems.propTypes = {
    items: PropTypes.array,
    isBasketPage: PropTypes.bool
};

DisplayItems.defaultProps = {
    isBasketPage: false
};

const DisplayItemsWithRouter = withRouter(DisplayItems);

export default DisplayItemsWithRouter;

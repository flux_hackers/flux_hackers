import React from 'react';
import './intro.css';
import '../../globalCss/colours.css';
import { Link } from 'react-router-dom';
import logo from '../../images/cheese-grater.png';

class Intro extends React.Component {
    constructor() {
        super();

        this.state = {
            name: ''
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({
            name: event.target.value
        })
    }

    render() {
        return (
            <React.Fragment>
                <div className="headerContainer">
                    <h1>Grate shopper</h1>
                    <img className="logo" src={logo}/>
                </div>
                <div className="mainSiteContainer">
                    <p>This is your chance to become a shopping pro and save some money towards something you love.</p>
                    <form>
                        <label>Please enter your name:</label><input value={this.name} onChange={this.handleChange}/>
                    </form>
                    <p>Now click on the subject you'd like to test your shopping skills on.</p>
                    <div className="introShoppingContainer">
                        <Link to={`/${this.state.name}/grocery`} className="introShoppingListBox introShoppingListBox-grocery background-blue">
                            <span className="introShoppingLabel">Groceries</span>
                        </Link>
                        <div className="introShoppingListBox introShoppingListBox-clothes background-green">
                            <span className="introShoppingLabel">Clothes</span>
                        </div>
                        <div className="introShoppingListBox introShoppingListBox-holiday background-pink">
                            <span className="introShoppingLabel">Holiday</span>
                        </div>
                        <div className="introShoppingListBox introShoppingListBox-insurance background-yellow">
                            <span className="introShoppingLabel">Car Insurance</span>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
};

export { Intro }

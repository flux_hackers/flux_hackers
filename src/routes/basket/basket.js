import PropTypes from 'prop-types';
import React from 'react';
import DisplayItems from '../../components/displayItems/displayItems';
import { ShoppingBasket } from '../../utilities/shopping-basket';
import logo from '../../images/cheese-grater.png';
import './basket.css';

class Basket extends React.Component {

    constructor() {
        super();

        this.emptyBasket = this.emptyBasket.bind(this);
    }

    emptyBasket(name) {
        ShoppingBasket.emptyBasket(name);
        this.forceUpdate();
    }

    render() {
        const { match } = this.props;
        const name = match.params.name;

        return (
            <React.Fragment>
                <div className="headerContainer">
                    <h1>Your Basket</h1>
                    <img className="logo" src={logo}/>
                </div>
                <div className="basketContainer">
                    <button onClick={() => this.emptyBasket(name)}>Empty Basket</button>
                    <p>Your Score: {ShoppingBasket.getScore(name)}</p>
                    <p>Your Total: {ShoppingBasket.getTotal(name)}</p>
                    <DisplayItems items={ShoppingBasket.getItems(name)} isBasketPage />
                </div>
            </React.Fragment>
        );
    }
}

Basket.propTypes = {
    match: PropTypes.object.isRequired
};

export { Basket }

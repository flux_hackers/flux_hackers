import React from 'react';
import DisplayItems from '../../components/displayItems/displayItems';
import GroceryInfo from './groceryInfo/groceryInfo';
import './grocery.css';
import logo from '../../images/cheese-grater.png';

const Grocery = () => (
    <React.Fragment>
        <div className="headerContainer">
            <h1>Grocery Shopping</h1>
            <img className="logo" src={logo}/>
        </div>
        <div className="Grocery-mainContainer mainSiteContainer">
            <div className="Grocery-listContainer">
                <GroceryInfo />
            </div>
            <DisplayItems />
        </div>
    </React.Fragment>
);

export { Grocery }

import React from 'react';
import { BrowserRouter } from "react-router-dom";
import './App.css';
import { Template } from './components/template/template';

const App = () => (
    <BrowserRouter>
        <Template />
    </BrowserRouter>
);

export default App;
